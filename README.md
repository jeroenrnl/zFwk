# zFwk
**The Z FrameWork**
*Jeroen Roos*

The **Z Framework** or **zFwk** is a framework that aims to be a complete set of base libraries to build a PHP application but without dicating the architecture of your application.
The Z Framework is derived from [Zoph](http://www.zoph.org), one of the oldest still actively developed PHP projects. 

The Z Framework currently consist of a _database query builder_, a _templating engine_, a _configuration engine_ and few generic classes. 

The **Z Framework** is in it's infancy and should **not** be used in production. If you want to consider using it, please let me know.
